/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Vicente
 */
public class Porcentaje extends Calculo{
    	double Porcentaje;
	
	public Porcentaje(double A, double B) {
        
        super(A, B, '%');
        this.Porcentaje = (B*100)/A;
        this.setResultado(this.Porcentaje);
    }
}
