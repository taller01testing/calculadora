public class Logaritmo extends Calculo{
	double Logaritmo;
	
	public Logaritmo(double A, double B) {
        
        super(A, B, '^');
        this.Logaritmo = Math.log(A);
        this.setResultado(this.Logaritmo);
    }
}