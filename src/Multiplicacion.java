
public class Multiplicacion extends Calculo {
	double Multiplicacion;
	
	public Multiplicacion(double A, double B) {
        
        super(A, B, '*');
        this.Multiplicacion = A * B;
        this.setResultado(this.Multiplicacion);
    }
}
