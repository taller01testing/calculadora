
public class RaizEnesima extends Calculo{
	double RaizEnesima;
	
	public RaizEnesima(double A, double B) {
        
        super(A, B, '^');
        this.RaizEnesima = Math.pow(A,(1/B));
        this.setResultado(this.RaizEnesima);
    }
}
