public class Potencia extends Calculo {
	double Potencia;
	
	public Potencia(double A, double B) {
        
        super(A, B, '*');
        this.Potencia = Math.pow(A,B);
        this.setResultado(this.Potencia);
    }
}