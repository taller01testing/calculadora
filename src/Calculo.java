//CLASE PADRE 

public class Calculo{
	//OPERANDOS
	private double NumA;
	private double NumB;
	//OPERADOR 
	private char Operador;
	//RESULTADO
	private double Resultado;
	
	//GET
	public double getNumA(){
		return NumA;
	}
	public double getNumB(){
		return NumB;
	}
	public char getOperador(){
		return Operador;
	}
	public double getResultado(){
		return Resultado;
	}
	
	//SET
	public void setNumA(double NumA){
		this.NumA = NumA;
	}
	public void setNumB(double NumB){
		this.NumB = NumB;
	}
	public void setOperador(char Operador){
		this.Operador = Operador;
	}
	public void setResultado(double Resultado){
		this.Resultado = Resultado;
	}
	
	public Calculo(double NumA, double NumB, char Operador) {
		this.NumA = NumA;
		this.NumB = NumB;
		this.Operador = Operador;
	}
	
	public void Calcular() {
		System.out.println("El resultado es: "+ this.Resultado);
	}
	
	
}	
	