import java.lang.Math;

public class Raiz extends Calculo{
	double Raiz;
	
	public Raiz(double A, double B) {
        
        super(A, B, '^');
        this.Raiz = Math.sqrt(A);
        this.setResultado(this.Raiz);
    }
}